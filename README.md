# OOP PHP Content Management System
## 

This is a basic framework to manage and generate dynamic content.

www.lite-cms.com

## Notes

- Responsive scaffolding from Twitter Bootstrap
- WYSIWYG Editor - summernote : http://hackerwins.github.io/summernote/ 
- Create/Edit/Add Pages
- Create/Edit/Add Users
- Clean URLs 
- Routing 
- Flexible Database or Static .phtml content
- Configuration settings in the config.php file
- Make sure you see the .htaccess file (It's hidden on Linux/Mac)

## Dependencies

- Twitter Bootstrap 3.0
- jQuery/Validator
- Font Awesome

## Thanks
- Jesse Boyer (<http://jream.com>) for basic CRUD class

***
## 
Copyright (C), 2013 David Bergunder