<?php
/**
 * Admin Dashboard Class, extends page
 *
 * Responsible for:
 *  - Page Creation / Editing / Toggle / Deleting
 *  - User Creation / Editing / Toggle / Deleting
 *  - Settings
 *  - Log
 * 
 * Filename: dashboard.php
 * Description: Administrative Tools for the CMS
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class dashboard extends Page {

	//Admin Access
	protected $_role = Enum::ADMIN;

	//Page Creation Filter
	protected $_reservedNames = array();

	/**
	 * Verifys user is logged in and has admin privledges
	 */
	public function __construct(){
		//Check for user loggin/Authorization before calling parent construct
		if(User::is_logged()){
			//Check user role
			$auth = new Auth(User::getRole());

			if (!$auth->hasAuth($this->_role)){
				$this->_error = "User does not have Admin privledges";
				//Load the home page
				$page = new index();
				$page->view();
				//Stop the current page from loading
				exit();
			}
		}
		else{
			//GoTo login
			$page = new login();
			$page->view();
			//Stop the current page from loading
			exit();
		}

		//Generate Reserved List of blacklisted page names
		$this->reservedList();

		//Call parent class
		parent::__construct();
	}

	//Load Dashboard Content from file
	//Preview general statistics
	protected function loadContent(){
		//Get count of how many users
		$this->_dBase->table = "users";
		$query = $this->_dBase->select(array('user_id'));

		$userCount = count($query);

		//Get count of how many pages
		$this->_dBase->table = "pages";
		$query = $this->_dBase->select(array('page_id'));

		$pageCount = count($query);

		//How many modules are installed
		//Array Count of returned routes from Module that match Admin
		$moduleCount = count(preg_grep("/admin/", Loader::routes()));

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '.phtml');
	}

	//Load Dashboard Navigation
	protected function loadNav(){
		//parent::loadNav();
		$nav = new Nav();
		print $nav->admin();
	}

	//Load Dashboard Header
	protected function loadHeader(){
		//parent::loadHeader();
		require_once(ROOT_PATH. DS . ADMIN_DIR . DS .'views' . DS . 'templates' . DS . '_header.phtml');
	}

	//Load Dashboard Footer
	protected function loadFooter(){
		//parent::loadFooter();
		require_once(ROOT_PATH. DS . ADMIN_DIR . DS .'views' . DS . 'templates' . DS . '_footer.phtml');
	}

	/**
	 * ***************************************************************************
	 * 
	 * Dashboard Settings:
	 * - View all Settings
	 * - Add Setting
	 * - Edit Setting
	 * - Delete Setting
	 *
	 * ***************************************************************************
	 */	
	
	/**
	 * View defined constants as settings
	 */
	public function settings(){
		$this->_dBase->table = 'settings';
		$settings = $this->_dBase->select(array('setting_id', 'description', 'value'));

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
	}

	/**
	 * Add Setting
	 */
	public function addSetting(){
		//Set Database to settings table
		$this->_dBase->table = 'settings';

		//Submiting new setting
		if($_POST){
			//Name, Descriptiong, and Value fields are set
			if(!empty($_POST['name']) && !empty($_POST['description']) && !empty($_POST['value'])){

				//Check for existing setting with name in database
				$query = $this->_dBase->select(array('setting_id', 'name'), array('name' => $_POST['name']));

				//name must be unique
				if (isset($query[0])){
					$this->_error = 'Setting:'. $query[0]['name'] . ', ID: '. $query[0]['setting_id'] .' already exists.';
				}else{
					$this->_dBase->insert(array('name' => $_POST['name'], 
												'description' => $_POST['description'], 
												'value' => $_POST['value']));
					//Load dashboard_settings.phtml
					$this->settings();
					return true;
				}
			}
			else{
				$this->_error = 'Name, Description and Value must be set.';
			}
		}

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
	}

	/**
	 * Edit Setting
	 */
	public function editSetting(){
		//Set Database table to settings
		$this->_dBase->table = 'settings';
		//Get URL parameters
		$params = Url::getAll();
		//Setting Params
		$setting = '';

		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check to see if its in the database
			$query = $this->_dBase->select(array('name', 'description', 'value'), array('setting_id' => $params['id']));
			//Make sure DB entry exists
			if(isset($query[0])){
				$setting = $query[0];

				if($_POST){
					//Passwords must match, and email can not be blank.
					if(!empty($_POST['name']) && !empty($_POST['description']) && !empty($_POST['value']) ){

						if($this->_dBase->update(array(
													'name' => $_POST['name'],
													'description' => $_POST['description'],
													'value' => $_POST['value']) , 
													array('setting_id' => $params['id']))){
							$this->_success = "Setting Updated";
							$this->settings();
							return true;
						}
						else{
							$this->_error = "Setting could not be updated.  Please contact your website administrator";
						}

					}
					else{
						$this->_error = "Please verify fields are not blank.";
					}
				}

				require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
			}
			else{
				$this->_error = "Setting ID not found.";
				$this->settings();
				return false;
			}
		}
		else{
			$this->_error = "Invalid parameters passed to Edit User";
			$this->settings();
			return false;
		}
	}

	/**
	 * Deletes Setting
	 */
	public function deleteSetting(){
		//Set Database table to settings
		$this->_dBase->table = 'settings';
		//Get URL parameters
		$params = Url::getAll();

		//Make sure params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check for Instance in Database
			$query = $this->_dBase->select(array('name'), array('setting_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Setting ID not found.";
			}
			else{
				//Delete setting from Database
				$this->_dBase->delete(array('setting_id' => $params['id']));
				$this->_success = $query[0]['name']. " succesfully deleted.";
			}
		}
		else{
			$this->_error = "Invalid parameters for Setting deletion.";
		}
		//Return to dashboard_settings
		$this->settings();	
	}

	/**
	 * ***************************************************************************
	 * 
	 * Dashboard Page Controls:
	 * - View all Pages
	 * - Add Page
	 * - Edit Page
	 * - Enable/Disable Page
	 * - Delete Page
	 *
	 * ***************************************************************************
	 */
	
	/**
	 * Lists all admin accessible pages
	 * @return bool false
	 */
	public function pages(){
		$this->_dBase->table = 'pages';
		$pages = $this->_dBase->select(array('page_id', 'title', 'name', 'enabled', 'author_id', 'edit_date'));

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
	}

	/**
	 * Add a new Page
	 * Generates class, inherits from Page class in /pages/ folder.
	 * Creates Database Entry for Content Storage.
	 * file_put_contents('filename.jpg', base64_decode($base64string));
	 *
	 * Note: WYSIWYG Editor converts special characters to ascii codes
	 * 
	 */
	public function addPage(){
		//Set Database call to pages table
		$this->_dBase->table = 'pages';

		//If we are posting a new page
		if($_POST){
			//Title and Name fields set, and not a reserved name
			if(!empty($_POST['name']) && !empty($_POST['title']) && !in_array($_POST['name'], $this->_reservedNames )){

				//sanitize spaces and strange characters
				$_POST['name'] = $this->nameCheck($_POST['name']);

				//Check for existing page with name in database
				$query = $this->_dBase->select(array('page_id', 'name', 'title'), array('name' => $_POST['name']));

				//Name must be unique
				if (isset($query[0])){
					//TODO: Persist Content
					$this->_error = 'Page Named:'. $query[0]['name'] . ', '. $query[0]['title'] .' already exists.';
				}
				else{
					//Create Page
					
					//TODO: Generate Information - Creation Data, Include Title As Description
					//TODO: Move to template file for easy editing.
					//Generate Page class file
					$data = 	"<?php".PHP_EOL;
					$data .=	"".PHP_EOL;
					$data .=	"class ". $_POST['name'] ." extends Page {".PHP_EOL;
					$data .=	"".PHP_EOL;
					$data .=	"	public function __construct(){".PHP_EOL;
					$data .=	"		parent::__construct();".PHP_EOL;
					$data .=	"	}".PHP_EOL;
					$data .=	"}".PHP_EOL;
					$data .=	"".PHP_EOL;
					$data .= 	" /** EOF */ ";

					//Try to create the class.php file for the page.
					if (false === file_put_contents('../pages/'.$_POST['name'] .'.php', $data)){
						//Return access error to pages file
						//TODO: Persist Content
						$this->_error = 'Page could not be created.  Please chmod 777 to /pages/ folder';
					}
					else{
						//Insert into DB
						$query = $this->_dBase->insert(
							array(
								'name' =>  $_POST['name'], 
								'title' =>  $_POST['title'], 
								'author_id' => User::getId(), 
								'create_date' => date("Y-m-d H:i:s"), 
								'content' =>  $this->sanitize($_POST['content']), 
								'publish' => $_POST['publish'],
								'meta' =>  $_POST['meta']
							)
						);
						//Log Create Page
						$this->log($_POST['title'], $query, User::getId(), "Created Page");

						$this->_success = $_POST['title']. ' page created.';
					}

					//Load dashboard_pages.phtml
					$this->pages();
					return true;
				}
			}
			else{
				$this->_error = 'Both Page Name and Title must be set, and can not be a reserved name for the Core Website (index/login/dashboard.. etc).';
			}
		}

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
	}

	/**
	 * Edit existing page
	 * Note: Convert special characters to ascii codes
	 */
	public function editPage(){
		//Set Database call to pages table
		$this->_dBase->table = 'pages';
		//Check for params passed via Url
		$params = Url::getAll();
		//Holds Content
		$page = '';

		//Make sure params are set, not empty and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Does page exist in DB
			$query = $this->_dBase->select(array('page_id', 'name', 'enabled', 'author_id', 'edit_date', 'title', 'publish', 'content', 'meta'), array('page_id' => $params['id']));

			if(isset($query[0])){
				//Load content
				$page = $query[0];
				$page['content'] = $this->sanitize($page['content']);

				//Check for edit submit
				if($_POST){
					if(!empty($_POST['title'])){
						//Update page information in database
						if($this->_dBase->update(
							array(
								'title' => $_POST['title'], 
								'content' => $this->sanitize($_POST['content']), 
								'publish' => $_POST['publish'],
								'meta' =>  $_POST['meta']
								), 
							array('page_id' => $params['id'])
							)
						){
							//Return to /pages/
							$this->_success = "Page: ".$_POST['title']." successfully updated.";
							$this->pages();

							//Log Event
							$this->log($_POST['title'], $params['id'], User::getId(), "Updated Page");

							return true;
						}
						else{
							$this->_error = "Page could not be updated.  Please contact your website administrator.";
						}
					}
					else{
						$this->_error = "Title can not be empty.";
					}
				}

				require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
			}
			else{
				$this->_error = "Page ID: ". $params['id'] ." could not be loaded.  Please contact your website administrator.";
				//Go back to Pages
				$this->pages();
				return false;
			}			
		}
		else
		{
			$this->_error = "Invalid Page ID";
			//Go back to Pages
			$this->pages();
			return false;
		}
	}

	/**
	 * Toggle page from view
	 */
	public function togglePage(){
		//Set Database call to pages table
		$this->_dBase->table = 'pages';
		//Check for params passed via Url
		$params = Url::getAll();

		//Check to see if params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Toggle Enable from DB
			$query = $this->_dBase->select(array('enabled', 'title'), array('page_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Page could not be located.";
			}
			else{
				//Toggle
				switch($query[0]['enabled']) {
					case 0:
						//Update to 1
						$this->_dBase->update(array('enabled' => 1), array('page_id' => $params['id']));

						//Success and Log
						$this->log($query[0]['title'], $params['id'], User::getId(), "toggled Enabled");
						$this->_success = $query[0]['title']. " enabled.";
						break;
					case 1:
						//Update to 0
						$this->_dBase->update(array('enabled' => 0), array('page_id' => $params['id']));

						//Success and log
						$this->log($query[0]['title'], $params['id'], User::getId(), "toggled Disable");
						$this->_success = $query[0]['title']. " disabled.";
						break;
					default:
						$this->_error = "Problem updating Page ". $query[0]['title'];
				}
			}
		}
		else{
			$this->_error = "Invalid parameters for toggling Page.";
		}

		$this->pages();
	}

	/**
	 * Set page to publish or draft
	 */
	public function publishPage(){
		//Set Database call to pages table
		$this->_dBase->table = 'pages';
		//Check for params passed via Url
		$params = Url::getAll();

		//Check to see if params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Toggle Enable from DB
			$query = $this->_dBase->select(array('publish', 'title'), array('page_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Page could not be located.";
			}
			else{
				//Toggle
				switch($query[0]['publish']) {
					case ('publish'):
						//Update to draft
						$this->_dBase->update(array('publish' => 'draft'), array('page_id' => $params['id']));

						//Success and Log
						$this->log($query[0]['title'], $params['id'], User::getId(), "Set to Draft");
						$this->_success = $query[0]['title']. " set to draft.";
						break;
					case ('draft'):
						//Update to publish
						$this->_dBase->update(array('publish' => 'publish'), array('page_id' => $params['id']));

						//Success and log
						$this->log($query[0]['title'], $params['id'], User::getId(), "Set to published");
						$this->_success = $query[0]['title']. " published.";
						break;
					default:
						$this->_error = "Problem updating Page ". $query[0]['title'];
				}
			}
		}
		else{
			$this->_error = "Invalid parameters for toggling Page.";
		}

		$this->pages();
	}

	/**
	 * Deletes a Page
	 * Removes Class file in /pages/
	 * Removes Database Entry
	 */
	public function deletePage(){
		//Set Database call to pages table
		$this->_dBase->table = 'pages';
		//Check for params passed via Url
		$params = Url::getAll();

		//Check to see if params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Delete From Database
			$query = $this->_dBase->select(array('name', 'title'), array('page_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Page could not be located.";
			}
			else{
				//Delete
				$this->_dBase->delete(array('page_id' => $params['id']));

				//Delete File From /pages/
				$path = '../pages/'.$query[0]['name'] .'.php';

				//Is the file accessible
				if(realpath($path) && is_readable($path)){
					//Delete file
					if(!unlink($path)){
						$this->_error = "Page file could not be deleted";
					}
					else{
						//Log Delete Page
						$this->_success = $query[0]['title']. " succesfully deleted.";
						$this->log($query[0]['title'], $params['id'], User::getId(), "Deleted Page");
					}
				}
				else{
					$this->_error = "Page file could not be located.";
				}
			}//End Isset $query
		}
		else
		{
			$this->_error = "Invalid parameters for removing Page.";
		}//End Isset Params

		//Load dashboard_pages.phtml
		$this->pages();
	}

	/**
	 * ***************************************************************************
	 * 
	 * Dashboard User Controls:
	 * - View all Users
	 * - Add User
	 * - Edit User
	 * - Enable/Disable User
	 * - Delete User
	 *
	 * ***************************************************************************
	 */

	/**
	 * Lists all users
	 */
	public function users(){
		//Set Database call to users table
		$this->_dBase->table = 'users';

		$users = $this->_dBase->select(array('user_id', 'email', 'user_name', 'role', 'status'));

		if (!empty($users)){

			//Prettyfi the data set
			$users = $this->addRole($users);

			require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
		}
		else{
			$this->_error = "Users could not be loaded from the Database";			
		}
	}

	/**
	 * Add a new user
	 */
	public function addUser(){
		//Set Database to users table
		$this->_dBase->table = 'users';

		//Submiting new user
		if($_POST){
			//Email and password fields set
			if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password2']) && ($_POST['password'] === $_POST['password2'])){

				//Check for existing page with name in database
				$query = $this->_dBase->select(array('user_id', 'email'), array('email' => $_POST['email']));

				//Email must be unique
				if (isset($query[0])){
					$this->_error = 'User:'. $query[0]['email'] . ', ID: '. $query[0]['user_id'] .' already exists.';
				}
				//Register User
				else if(!$error = User::register($_POST['email'], $_POST['username'], $_POST['role'], $_POST['password'])){
					$this->_error = $error;
				}else{
					//Load dashboard_users.phtml
					$this->users();
					return true;
				}
			}
			else{
				$this->_error = 'Email must be set, and both passwords must match.';
			}
		}

		require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
	}

	/**
	 * Edit existing user
	 */
	public function editUser(){
		//Set Database table to users
		$this->_dBase->table = 'users';
		//Get URL parameters
		$params = Url::getAll();
		//User Params
		$user = '';

		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check to see if its in the database
			$query = $this->_dBase->select(array('email', 'user_name', 'user_id', 'role', 'salt'), array('user_id' => $params['id']));
			//Make sure DB entry exists
			if(isset($query[0])){
				$user = $query[0];

				if($_POST){
					//Passwords must match, and email can not be blank.
					if(($_POST['password'] === $_POST['password2']) && ($_POST['password'] != '') && ($_POST['email'] != '') ){

						if(User::userUpdate($params['id'], $_POST['email'], $_POST['username'], $_POST['role'], $_POST['password'], $user['salt'])){
							$this->_success = "User Updated";
							$this->users();
							return true;
						}
						else{
							$this->_error = "User could not be updated.  Please contact your website administrator";
						}

					}
					else{
						$this->_error = "Please verify password fields match and are not blank.";
					}
				}

				require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
			}
			else{
				$this->_error = "User ID not found.";
				$this->users();
				return false;
			}
		}
		else{
			$this->_error = "Invalid parameters passed to Edit User";
			$this->users();
			return false;
		}
	}

	/**
	 * Toggle user account
	 */
	public function toggleUser(){
		//Set Database call to pages table
		$this->_dBase->table = 'users';
		//Check for params passed via Url
		$params = Url::getAll();

		//Check to see if params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Toggle Enable from DB
			$query = $this->_dBase->select(array('status', 'email'), array('user_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "User could not be located.";
			}
			else{
				//Toggle
				switch($query[0]['status']) {
					case 0:
						//Update to 1
						$this->_dBase->update(array('status' => 1), array('user_id' => $params['id']));

						//Success
						$this->_success = $query[0]['email']. " user enabled.";
						break;
					case 1:
						//Update to 0
						$this->_dBase->update(array('status' => 0), array('user_id' => $params['id']));

						//Success
						$this->_success = $query[0]['email']. " user disabled.";
						break;
					default:
						$this->_error = "Problem updating User ". $query[0]['email'];
				}
			}
		}
		else{
			$this->_error = "Invalid parameters for toggling User.";
		}

		$this->users();
	}

	/**
	 * Delete User
	 * TODO: AJAX CALL
	 */
	public function deleteUser(){
		$this->_dBase->table = 'users';
		$params = Url::getAll();

		//Make sure params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check for Instance in Database
			$query = $this->_dBase->select(array('email'), array('user_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "User ID not found.";
			}
			else{
				//Delete user from Database
				$this->_dBase->delete(array('user_id' => $params['id']));
				$this->_success = $query[0]['email']. " succesfully deleted.";
			}
		}
		else{
			$this->_error = "Invalid parameters for User deletion.";
		}
		//Return to dashboard_users
		$this->users();
	}

	/**
	 * ***************************************************************************
	 * 
	 * Dashboard Log Controls:
	 * - View Logs
	 *
	 * ***************************************************************************
	 */
	
	/**
	 * View user login log
	 */
	public function userLogs(){
		$this->_dBase->table = 'user_logs';

		$logs = $this->_dBase->select(array('user_id', 'log_date'));

		if(isset($logs)){
			require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
		}
		else{
			$this->_error = "No User logs in Database";
		}

	}

	/**
	 * View page logs
	 */
	public function pageLogs(){
		$this->_dBase->table = 'page_logs';

		$logs = $this->_dBase->select(array('page_id', 'message', 'log_date'));

		if(isset($logs)){
			require_once(ROOT_PATH . DS . ADMIN_DIR . DS . 'views' . DS . __CLASS__ . '_' . __FUNCTION__ . '.phtml');
		}
		else{
			$this->_error = "No Page logs in the Database";
		}
	}

	/**
	 * ***************************************************************************
	 * 
	 * Dashboard Private Methods:
	 * - reserved name list
	 * - URL name check
	 * - Log
	 * - Clean content insert for special char
	 * ***************************************************************************
	 */
	
	//Populates reserved name list
	private function reservedList(){
		$this->_reservedNames[] = 'index';
		$this->_reservedNames[] = 'dashboard';
		$this->_reservedNames[] = 'error';
		$this->_reservedNames[] = 'login';
		$this->_reservedNames[] = 'register';
		$this->_reservedNames[] = 'sitemap';
		$this->_reservedNames[] = 'about';
		$this->_reservedNames[] = 'contact';
		$this->_reservedNames[] = 'privacy';
		$this->_reservedNames[] = 'profile';
		//$this->_reservedNames[] = '';
		
		//Modules are a reserved name as well
		$this->_reservedNames = array_merge($this->_reservedNames, Loader::routes());
	}

	/**
	 * Sanitzes and fixes names for URL
	 * @param  string $name name of page for URL
	 * 
	 * @return string       cleaned string
	 */
	private function nameCheck($name){
		//setlocale(LC_ALL, 'en_US.UTF8');
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $name);
		$clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_| -]+/", '_', $clean);

		return $clean;
	}

	/**
	 * Makes the data easier to read
	 * @param array $list 
	 * @return array 
	 */
	private function addRole($list){
		//Prettyfy the data set
		foreach($list as &$user){

			switch ($user['role']) {
			    case Enum::ADMIN:
			        //Add role_name as Admin
					$user['role_name'] = 'Admin';
			        break;
			    case Enum::USER:
			    	//Add role_name as User
			        $user['role_name'] = 'User';
			        break;
			}
		}
		return $list;
	}

	/**
	 * Creates a message and logs event
	 * @param  string $title   title of page
	 * @param  int $page_id id of page
	 * @param  int $user_id id of user
	 * @param  string $message logged message
	 */
	private function log($title, $page_id, $user_id, $message){
		$message .= ' - '. $title .' - page_id: '. $page_id .' user_id: '. $user_id;

		$log = new Log($this->_dBase, 'page_logs');
 		$log->insert($page_id, $message);
	}

	/**
	 * Sanitizes content insert for special char
	 * @param  string $html HTML
	 * @return string 
	 */
	protected function sanitize($html){
		return str_replace(array("«","»","©"), array("<i class='fa fa-angle-double-left'></i>", "<i class='fa fa-angle-double-right'></i>", "<span class='glyphicon glyphicon-copyright-mark'></span>"), $html);
	}

	//Testing functions
	public function test(){
		//echo $this->nameCheck('here is my text right-here OK!');
		//echo $this->sanitize('<div> « <br> » <br> © <br> &laquo; <br> &raquo; <br> &copy;</div>');
		//echo '<br> &laquo; <br> &raquo; <br> &copy;';
		//var_dump(User);
	}
}
/** EOF */