<?php
/**
 * Admin Help Class, extends dashboard
 *
 * Simple Help file for new users
 * 
 * Filename: help.php
 * Description: Administrative Tools for the CMS
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class help extends dashboard {

	//Load help Content from file
	protected function loadContent(){
		require_once(ROOT_PATH.'/admin/views/'.__CLASS__.'.phtml');
	}

}
 /** EOF */ 