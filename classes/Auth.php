<?php
/**
 * Auth Class
 * 
 * Filename: Auth.php
 * Description: Determines Authorization for users to view 
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Auth {

	/** @var array list of roles */
	private $_roles = array();
	/** @var int user role */
	private $_role;

	public function __construct($role){
		$this->_role = $role;

		$this->_roles = $this->setRoles();
	}

	/**
	 * Returns user role
	 * @return string user role
	 */
	public function getAuth(){
		return $this->_roles[$this->_role];
	}

	/**
	 * Checks to see if user has auth against page
	 * @param  int  $pageRole page auth role
	 * @return boolean         
	 */
	public function hasAuth($pageRole){
		return $this->_role == $pageRole? true : false;
	}

	//TODO: role table in database
	//Currently listed in config.php Enum Class
	//Generates list of roles
	private function setRoles(){
		$list = array(
			Enum::ADMIN => 'Admin',
			Enum::USER => 'User');

		return $list;
	}

}
/** EOF */