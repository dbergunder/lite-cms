<?php
/**
 * Core Class
 * 
 * Filename: Core.php
 * Description: Core Class initializes the site
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Core {

	/** @var Router Routes for site */
	private $_router;
	/** @var CRUD Database */
	private $_dBase;
	/** @var Loader Module Loader */
	private $_loader;

	public function __construct($router, $dBase){
		$this->_router = $router;
		$this->_dBase  = $dBase;
		$this->_loader = new Loader();
	}

	//Runs the application
	public function run() {
		//Start Buffer
		ob_start();

		$this->_router->addList(Loader::run());

		//Get the uri
		$url  = Url::get();

		//Route exists
		if (isset($url[0])) {
			//Check the Route
			$page = $this->_router->submit($url[0]);
			//Return the page
			$page = new $page();

			if(!$page->isPublished()) {
				$page = new index();
			}
		}
		else{
			//Forward to error 404 page
			$page = new error();
		}

		//Load Page
		$page->view();

		//Displays Page
		ob_get_flush();
	}

}
/** EOF */