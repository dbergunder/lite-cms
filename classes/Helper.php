<?php
/**
 * Helper Class
 * 
 * Filename: Helper.php
 * Description: Helper
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Helper {

	//Redirects the page
	public static function redirect($url, $statusCode = 303)
	{
	   header('Location: ' . $url, true, $statusCode);
	   die();
	}

}