<?php
/**
 * Module Loader Class
 * 
 * Filename: Loader.php
 * Description: Module Loader
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Loader {

	/** @var string Routes for modules */
	private static $_routes = array();
	/** @var string directory of module folder */
	private static $_dir;

	public function __construct(){
		self::$_dir = ROOT_PATH . DS . MODULE_DIR;
	}

	/**
	 * Loads the modules
	 * @return array loaded routes
	 */
	public static function run(){
		//Array of items in Modules folder
		$moduleFolder = array_diff(scandir(self::$_dir), array('..', '.'));

		//If module directory exists load bootloader
		foreach ($moduleFolder as $key => $value){
			if(is_dir(self::$_dir . DS . $value)){
				require_once(self::$_dir . DS . $value . DS . "bootloader.php");

				//Add path to includes
				set_include_path(implode(
					PATH_SEPARATOR, array(
						realpath(self::$_dir . DS . $value),
						get_include_path()
						)
					));
			}
		}
		//Return all loaded routes
		return self::$_routes;
	}

	//List the modules
	public static function routes(){
		return self::$_routes;		
	}

}
/** EOF */