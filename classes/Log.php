<?php
/**
 * Log Class
 * 
 * Filename: Log.php
 * Description: Logs Events like user login and page edits
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

 class Log {

 	//Database
 	protected $_dBase;
 	//Table name
 	protected $_table;

 	/**
 	 * Requires Database object and destination Table name (ex user_logs)
 	 * @param CRUD $dBase 
 	 * @param string $table Table Name
 	 */
 	public function __construct($dBase, $table){
 		$this->_dBase = $dBase;
 		$this->_table = $table;
 	}

 	//TODO: Refactor and design better
 	/**
 	 * Inserts Row into correct log table
 	 * @param  int $id      id of the target
 	 * @param  string $message message to be stored
 	 * @return bool          success or failure
 	 */
 	public function insert($id, $message = null){
 		$this->_dBase->table = $this->_table;

 		//Page Log Table
 		if(isset($message)){
			if($this->_dBase->insert(array('page_id' => $id, 'message' => $message))){
 				return true;
 			}
 			else
 				return false;
 		}
 		//User Log Table
 		else{
 			if($this->_dBase->insert(array('user_id' => $id))){
 				return true;
 			}
 			else
 				return false;
 		}
 	}
}
/** EOF */