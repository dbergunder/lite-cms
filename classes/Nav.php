<?php
/**
 * Nav Class
 * 
 * Filename: Nav.php
 * Description: Generates a Navigation
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Nav {
	/** @var Router Web Site Routes */
	private $_router = null;
	/** @var CRUD Database Connection */
	private $_dBase = null;

	/**
	 * Main Navigation
	 * @param  CRUD $dBase Database Object
	 * @return string HTML
	 */
	public function view($dBase){

		if(isset($dBase)){
			$this->_dBase = $dBase;
			$this->_dBase->table = 'pages';
		}
		else
		{
			return '<div class="alert alert-danger"><strong>Error!</strong> Could not load Navigation.</div>';
		}

		//Only return pages that are enabled
		$pages = $this->_dBase->select(array('name', 'title'), array('enabled'=> 1));

		//Installed Public Modules
		//TODO: Order?
		$links = $this->getPublicModules();
		if(!empty($links)){
			foreach($links as $key => $value){
				$pages[] = array('name' => $value, 'title' => ucfirst($value));
			}
		}

		//Generate Main Navigation markup
		$html  = '  <!-- Static navbar -->'.PHP_EOL;
		$html .= '      <div class="navbar navbar-default">'.PHP_EOL;
		$html .= '        <div class="navbar-header">'.PHP_EOL;
		$html .= '          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '          </button>'.PHP_EOL;
		$html .= '          <a class="navbar-brand" href="'.SITE_URL.'/"><i class="fa fa-home"></i> '.SITE_NAME.'</a>'.PHP_EOL;
		$html .= '        </div>'.PHP_EOL;
		$html .= '        <div class="navbar-collapse collapse">'.PHP_EOL;
		$html .= '          <ul class="nav navbar-nav">'.PHP_EOL;

		//Generate Link markup from page names
		foreach ($pages as $row){
			if ($row['name'] != 'index'){
		$html .= '            <li><a href="'.SITE_URL.'/'. $row['name']. '" class="main-nav">'. $row['title'] .'</a></li>'.PHP_EOL;
			}
		}

		$html .= '          </ul>'.PHP_EOL;
		$html .= '        </div><!--/.nav-collapse -->'.PHP_EOL;
		$html .= '     </div>'.PHP_EOL;

		return $html;
	}

	/**
	 * Admin Navigation
	 * @return string HTML
	 */
	public function admin(){
		//Generate Admin Navigation markup
		$html  = '  <!-- Static navbar -->'.PHP_EOL;
		$html .= '      <div class="navbar navbar-default">'.PHP_EOL;
		$html .= '        <div class="navbar-header">'.PHP_EOL;
		$html .= '          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '            <span class="icon-bar"></span>'.PHP_EOL;
		$html .= '          </button>'.PHP_EOL;
		$html .= '          <a class="navbar-brand" href="'.SITE_URL.'/dashboard/">Dashboard</a>'.PHP_EOL;
		$html .= '        </div>'.PHP_EOL;

		//Admin Links
		$html .= '        <div class="navbar-collapse collapse">'.PHP_EOL;
		$html .= '          <ul class="nav navbar-nav">'.PHP_EOL;
		$html .= '            <li><a href="'.SITE_URL.'/dashboard/pages" class="main-nav">Pages</a></li>'.PHP_EOL;
		$html .= '            <li><a href="'.SITE_URL.'/dashboard/users" class="main-nav">Users</a></li>'.PHP_EOL;
		$html .= '			<li class="dropdown">'.PHP_EOL;
		$html .= '					    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.PHP_EOL;
		$html .= '                        Logs';
		$html .= '					      <b class="caret"></b></a>'.PHP_EOL;
		$html .= '					    <ul class="dropdown-menu">'.PHP_EOL;
		$html .= '					      <li><a href="'.SITE_URL.'/dashboard/userLogs/">User Logs</a></li>'.PHP_EOL;
		$html .= '					      <li><a href="'.SITE_URL.'/dashboard/pageLogs/">Page Logs</a></li>'.PHP_EOL;
		$html .= '					    </ul>'.PHP_EOL;
		$html .= '				</li>'.PHP_EOL;

		//Check for Admin Modules
		$links = $this->getAdminModules();
		if(!empty($links)){

		$html .= '			<li class="dropdown">'.PHP_EOL;
		$html .= '					    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.PHP_EOL;
		$html .= '                        Modules';
		$html .= '					      <b class="caret"></b></a>'.PHP_EOL;
		$html .= '					    <ul class="dropdown-menu">'.PHP_EOL;

			foreach($links as $key => $value){

		$html .= '					      <li><a href="'.SITE_URL.'/'.$value.'/">'.preg_replace("/admin/", '', $value).'</a></li>'.PHP_EOL;	

			}

		$html .= '					    </ul>'.PHP_EOL;

		}

		$html .= '          </ul>'.PHP_EOL;

		//Name/Profile/Logout Links
		$html .= '		  <ul class="nav navbar-nav navbar-right">'.PHP_EOL;
		$html .= '          <li><a href="'.SITE_URL.'/help/" class="main-nav" data-toggle="tooltip" title="Documentation"><i class="fa fa-book"></i></a></li>'.PHP_EOL;
		$html .= '          <li><a href="'.SITE_URL.'/" class="main-nav" data-toggle="tooltip" title="Go to Home Page"><i class="fa fa-home"></i></a></li>'.PHP_EOL;
		$html .= '			<li class="dropdown">'.PHP_EOL;
		$html .= '					    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.PHP_EOL;
		$html .= 					      User::getUsername();
		$html .= '					      <b class="caret"></b></a>'.PHP_EOL;
		$html .= '					    <ul class="dropdown-menu">'.PHP_EOL;
		$html .= '					      <li><a href="'.SITE_URL.'/profile/"><i class="fa fa-globe"></i> Profile</a></li>'.PHP_EOL;
		$html .= '					      <li><a href="'.SITE_URL.'/login/logout/"><i class="fa fa-sign-out"></i> Logout</a></li>'.PHP_EOL;
		$html .= '					    </ul>'.PHP_EOL;
		$html .= '				</li>'.PHP_EOL;
		$html .= '			</ul>'.PHP_EOL;
		$html .= '        </div><!--/.nav-collapse -->'.PHP_EOL;
		$html .= '     </div>'.PHP_EOL;

		return $html;		
	}

	private function getPublicModules(){
		$links = Loader::routes();

		//If there are modules routed return and remove admin
		if(!empty($links)){
			$links = preg_grep("/admin/", $links, PREG_GREP_INVERT);
			$links = preg_grep("/Feed/", $links, PREG_GREP_INVERT);
		}
		
		return $links;
	}

	private function getAdminModules(){
		$links = Loader::routes();

		//If there are modules routed return and keep admin
		if(!empty($links)){
			$links = preg_grep("/admin/", $links);
			$links = preg_grep("/Feed/", $links, PREG_GREP_INVERT);
		}
		
		return $links;
	}
}
/** EOF */

