<?php
/**
 * Page Class
 * 
 * Filename: Page.php
 * Description: Base class for all pages
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Page {
	/** @var array Holds additional parameters */
	protected $_params = array();

	/** @var CRUD Database Connection */
	protected $_dBase = null;

	/** View Properties */
	protected $_title = '';
	protected $_content = '';
	protected $_meta = '';
	protected $_nav = '';
	protected $_cache = null;

	/**
	 * CSS Emedd Array
	 * @var array
	 * @usage $this->_css = '<link href="" rel="stylesheet">';
	 */
	protected $_css = array();

	/**
	 * JS Embedd Array
	 * @var array
	 * @usage $this->_js[] = '<script src="" type="text/javascript"></script>'
	 */
	protected $_js = array();

	/** @var array list of class methods that can be executed */
	protected $_methods = array();

	/** @var string Error Messages that can be displayed */
	protected $_error = null;

	/** @var string Success Messages that can be displayed */
	protected $_success = null;

	//Constructor
	public function __construct(){
		$this->_params = Url::get();
		
		//TODO: Pass through Core Class
		global $dBase;
		$this->_dBase = $dBase;

		//Load available class methods
		$this->_methods = get_class_methods($this);
	}

	//Load Method
	protected function loadMethod(){
		//Check for method arguement
		//If the method exists and isn't blank
		if (isset($this->_params[1]) && $this->_params[1] != ''){
			foreach($this->_methods as $key => $value)
			{
				if(strtolower($value) == strtolower($this->_params[1])){
					$method = $this->_params[1];
					//Execute the method
					$this->$method();	
					return true;			
				}
			}
		}
		return false;
	}

	//Loads the header file
	protected function loadHeader(){
		//Force utf-8
		header('Content-Type: text/html; charset=utf-8');
		
		$css = $this->loadCss();
		require_once('_header.phtml');
	}

	//Loads the footer file
	protected function loadFooter(){
		$js = $this->loadJs();
		require_once('_footer.phtml');
	}

	//Loads the Nav
	//TODO: Pass HTML to print, don't print to function
	protected function loadNav(){
		$nav = new Nav();
		$this->_nav = $nav->view($this->_dBase);
		print $this->_nav;
	}

	//Load Page content
	//@note Override this method to pull content from a phtml file instead
	protected function loadContent(){
		print $this->_content;
	}

	//Set up the page and variables
	protected function run(){
		//Use cache to deflect DB call if exists
		if(is_null($this->_cache)) {
			//Sets to use the pages table
			$this->_dBase->table = 'pages';
			//Select Page
			$query = $this->_dBase->select(array('page_id, title, content, meta'), array('name' => get_class($this)));

			//Load title, meta, and content
			if(isset($query[0])){
				$this->_title = $query[0]['title'];
				$this->_meta = $query[0]['meta'];
				$this->_content = $query[0]['content'];
			}
		}
		else {
			//Load title, meta, and content
			$this->_title = $this->_cache['title'];
			$this->_meta = $this->_cache['meta'];
			$this->_content = $this->_cache['content'];
		}
	}

	//Bool check to see if the page is published or not
	public function isPublished(){
		$query = $this->_dBase->select(array('publish','page_id, title, content, meta'), array('name' => get_class($this)));

		if(isset($query[0]) && $query[0]['publish'] == 'publish'){
			$published = true;
			$this->_cache = $query[0];
		}
		else {
			$published = false;
		}

		return $published;
	}

	//Generates the view
	public function view(){
		//Grab the query from the Database
		$this->run();

		$this->loadHeader();
		$this->loadNav();

		//Load possible methods
		//If false, loads main content
		if(!$this->loadMethod()){
			$this->loadContent();
		}

		$this->loadFooter();
	}

	//Generate CSS Style embed string
	private function loadCss(){
		$html = '';
		foreach($this->_css as $value){
			$html .= $value.PHP_EOL;
		}

		return $html;
	}

	//Generate JS Script embed string
	private function loadJs(){
		$html = '';
		foreach($this->_js as $value){
			$html .= $value.PHP_EOL;
		}

		return $html;
	}
}
/** EOF */