<?php
/**
 * Router Class
 * 
 * Filename: Router.php
 * Description: Routes the Request
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Router {
	//default route - config file 
	private $_default = DEFAULT_ROUTE;

	//collection of routes
	private $_routes = array();

	/**
	 * Override default route
	 * @param string $default
	 */
	public function setBasePath($default){
		$this->_default = $default;
	}

	/**
	 * Add new route
	 * @param string $route
	 */
	public function add($route){
		$this->_routes[] = $route;
	}

	/**
	 * Add route array
	 * @param array $routes
	 */
	public function addList($routes){
		$this->_routes = array_merge($this->_routes, $routes);
	}

	/**
	 * Returns array of routes
	 * @return array
	 */
	public function getRoutes(){
		return $this->_routes;
	}

	/**
	 * Routes by comparing against Url object
	 * @param  string $url
	 * 
	 * @return bool
	 */
	public function submit($url){
		//if isset and routed, otherwise go to default route
		$url = strtolower($url);
		foreach ($this->_routes as $key => $value){
			$route = strtolower($value);
			if (preg_match("#^$route$#", $url)){
				return $value;
			}
		}
		//Return false for future error handling
		return $this->_default;
	}

}
/** EOF */