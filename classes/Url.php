<?php
/**
 * Url Class
 * 
 * Filename: Url.php
 * Description: Singleton, handles the clean URI request
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class Url {
	/** @var Url Self */
	private static $_instance = null;
	/** @var array List of Parameters */
	private static $_params = array();
	/** @var array Request Array */
	private static $_request = array();

	/**
	 * Singleton Pattern
	 * 
	 * @return self
	 */
	public static function getInstance(){
		if(is_null(self::$_instance)){
			self::$_instance = new Url;
		}
		return self::$_instance;
	}

	//Constructor
	private function __construct() {
		self::$_request = (explode('/', self::removeLeadSlash($_SERVER['REQUEST_URI'] )));
	}

	//Returns array from clean URI
	public static function get(){
		return self::$_request;
	}

	/**
	 * Returns value from position of array if it exists, False if doesn't exist
	 * @param  int $key
	 * 
	 * @return mixed|bool
	 */
	public static function getPosition($key){
		 return isset(self::$_request[$key]) && self::$_request[$key] != "" ?
		 	self::$_request[$key] : false;
	}

	/**
	 * Additional Param Handling, False if none were found
	 * @param  string $par
	 * 
	 * @return mixed|bool
	 */
	public static function getParam($par){
		return isset($_GET[$par]) && $_GET[$par] != "" ?
			$_GET[$par] : false;
	}

	/**
	 * Return all additional Param, false if none were passed
	 * 
	 * @return array
	 */
	public static function getAll(){
		//Check for GET
		if (!empty($_GET)) {
			foreach($_GET as $key => $value){
				if(!empty($value)) {
					self::$_params[$key] = $value;
				}
			}
		}
		//Check for URI 
		//TODO: Properly identify params in URI
		else if(isset(self::$_request[1]) && isset(self::$_request[2]) && !isset(self::$_request[3])){
			self::$_params[self::$_request[1]] = self::$_request[2];
		}
		else if(isset(self::$_request[2]) && isset(self::$_request[3]))
		{
			self::$_params[self::$_request[2]] = self::$_request[3];
		}
		return self::$_params;
	}

	//Internal Methods
	/**
	 * Removes Trailing Slash
	 * @param  string $string
	 * 
	 * @return string
	 */
	private function removeTrailSlash($string){
		return $string[strlen($string) - 1] == '/' ? rtrim($string, '/') : $string;
	}

	/**
	 * Removes Leading Slash
	 * @param  string $string
	 * 
	 * @return string
	 */
	private function removeLeadSlash($string){
		return $string[0] == '/' ? ltrim($string, '/') : $string;
	}

}
/** EOF */