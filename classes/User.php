<?php
/**
 * User Class
 *
 * User Roles Enum:  int/config.php
 * 
 * Filename: User.php
 * Description: Access level to the site
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

 class User {
 	/** @var Url Self */
	private static $_instance 	= null;
 	/** @var CRUD Database Connection */
 	private static $_dBase 		= null;

 	//User Properties
 	/** @var int */
 	private static $_id 	 	= null;
 	/** @var string */ 	
 	private static $_email		= null;
 	/** @var string */
 	private static $_username	= null;
 	/** @var int */ 	
 	private static $_role  		= null;

 	//Constructor
	public function __construct($dBase){

		if(isset($dBase)){
			self::$_dBase = $dBase;
		}

		//If the user is logged in still update the object
		if(self::is_logged()){
 			self::setId($_SESSION['user']['id']);
 			self::setEmail($_SESSION['user']['email']);
 			self::setRole($_SESSION['user']['role']);
 			self::setUsername($_SESSION['user']['username']);
		}
	}

	/**
	 * Singleton Pattern
	 * @param CRUD $dBase
	 * 
	 * @return self
	 */
	public static function getInstance($dBase){
		if(is_null(self::$_instance)){
			self::$_instance = new User($dBase);
		}
		return self::$_instance;
	}
 	
 	/**
 	 * TODO: Remember Me Cookie
 	 * login, Start User Session
 	 * @param  string $email
 	 * @param  string $password
 	 *  
 	 * @return bool           
 	 */
 	public static function login($email, $password){
 		self::$_dBase->table = 'users';

 		//Get PW and SALT from DB
 		$query = self::$_dBase->select(array('user_id', 'password', 'salt', 'email', 'user_name', 'role'), array('email' => $email));

 		//Set class properties
 		if (!empty($query)){
 			//Check Database Hashed PW against submitted PW
 			if($query[0]['password'] === self::hash($password, $query[0]['salt'])){
	 			//Log Event
	 			$log = new Log(self::$_dBase, 'user_logs');
	 			$log->insert($query[0]['user_id']);

	 			self::setId($query[0]['user_id']);
	 			self::setEmail($query[0]['email']);
	 			self::setRole($query[0]['role']);
	 			self::setUsername($query[0]['user_name']);

	 			$user['id'] = $query[0]['user_id'];
	 			$user['email'] = $query[0]['email'];
	 			$user['role'] = $query[0]['role'];
	 			$user['username'] = $query[0]['user_name'];

	 			$_SESSION['user'] = $user;
	 			return true;
 			}
 			else{
 				//Password is invalid
 				return false;
 			}
 		}
 		else{
 			//Email is invalid
 			return false;
 		}	
 	}
 	/**
 	 * Is the user logged in
 	 * @return boolean
 	 */
 	public static function is_logged(){
 		return isset($_SESSION['user']) ? true : false;
 	}

 	/**
 	 * Register User
 	 * @param  string $email
 	 * @param  string $username
 	 * @param  string $password
 	 * @param  constant $role Enum
 	 * 
 	 * @return $user|bool
 	 */
 	public static function register($email, $username, $role, $password){
 		self::$_dBase->table = 'users';
 		//Is email address already registered?
 		$query = self::$_dBase->select('email', array('email' => $email));

 		//If available, register user.
 		if (!isset($query[0])){
 			//Generate Salt
 			$salt = self::createSalt($email.time());

 			//Create User
 			if(self::$_dBase->insert(array('email' => $email, 'user_name' => $username, 'role' => $role, 'password' => self::hash($password, $salt), 'salt' => $salt))){
 				return true;
 			}
 			else{
 				//Did not insert correctly
 				return "The User did not insert correctly.";
 			}
 		}
 		else{
 			//The email is already registered
 			return "The email: ". $email ." is already registered";
 		}	
 	}

 	//LogOut, Remove User Session
 	public static function logout(){
 		unset($_SESSION['user']);
 	}

 	//Reset Password
 	public static function resetPassword($email){
 		//Check for email in dBase
 		//If true, generate new PW (timestamp?)
 			//Update user with new PW
 			//Email user with new PW
 		//If false, user not recognized exception
 	}

 	/**
 	 * Admin Reset
 	 * @param  int $id       
 	 * @param  string $email    
 	 * @param  string $username 
 	 * @param  constant $role     Enum
 	 * @param  string $password 
 	 * @param  string $salt     
 	 * @return bool 
 	 */
 	public static function userUpdate($id, $email, $username, $role, $password, $salt){
		self::$_dBase->table = 'users';
 		return self::$_dBase->update(array('email' => $email, 'user_name' => $username, 'role' => $role, 'password' => self::hash($password, $salt)), array('user_id' => $id)) ? true : false;
 	}

	/**
	 * ***************************************************************************
	 * 
	 * Property Getters
	 *
	 * ***************************************************************************
	 */
	
 	/**
 	 * Get User ID
 	 * @return int
 	 */
 	public static function getId(){
 		return self::$_id;
 	}
 	/**
 	 * Get User Email
 	 * @return string 
 	 */
 	public static function getEmail(){
 		return self::$_email;
 	}
 	/**
 	 * Get Username
 	 * @return string 
 	 */
 	public static function getUsername(){
 		return self::$_username;
 	}
 	/**
 	 * Get User Role
 	 * @return int 
 	 */
 	public static function getRole(){
 		return self::$_role;
 	}

	/**
	 * ***************************************************************************
	 * 
	 * Property Setters
	 *
	 * ***************************************************************************
	 */
	
 	/**
 	 * Set User ID
 	 * @param int $id 
 	 */
 	private static function setId($id){
 		self::$_id = $id;
 	}
 	/**
 	 * Set User Email
 	 * @param string $email 
 	 */
 	private static function setEmail($email){
 		self::$_email = $email;
 	}
 	/**
 	 * Set Username
 	 * @param string $username 
 	 */
 	private static function setUsername($username){
 		self::$_username = $username;
 	}
 	/**
 	 * Set User Role
 	 * @param int $role
 	 */
 	private static function setRole($role){
 		self::$_role = $role;
 	}

 	/**
 	 * Update Password
 	 * @param  string $password 
 	 * @return bool           
 	 */
 	private static function updatePassword($password, $salt){
 		self::$_dBase->table = 'users';
 		return self::$_dBase->update(array('password' => self::hash($password, $salt)), array('user_id' => self::$_id)) ? true : false;
 	}

 	/**
 	 * Hash password
 	 * @param  string $password 
 	 * 
 	 * @return string           Hashed Password in ripemd160
 	 */
 	private static function hash($password, $salt){
 		return hash('ripemd160', $password.$salt);
 	}

 	/**
 	 * Generate Custom Salt
 	 * @param  string $salt base string to salt
 	 * @return string       hashed salt
 	 */
 	private static function createSalt($salt){
 		return hash('ripemd160', $salt);
 	}

 }
 /** EOF */