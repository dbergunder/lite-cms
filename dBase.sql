SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `lite_cms` DEFAULT CHARACTER SET utf8 ;
USE `lite_cms` ;

-- -----------------------------------------------------
-- Table `lite_cms`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lite_cms`.`users` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(40) NOT NULL ,
  `salt` VARCHAR(40) NOT NULL ,
  `role` INT(11) NOT NULL DEFAULT '1' ,
  `status` INT(11) NOT NULL DEFAULT '1' ,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `user_name` VARCHAR(45) NOT NULL DEFAULT 'Guest' ,
  PRIMARY KEY (`user_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

/*
'1', 'admin@site.com', 'dbe0005933ed0b81aea18bef92301b314ec3b8d4', '0f826d1f7a30edd8fb59e231675908e6db58f7fc', '0', '1', '0000-00-00 00:00:00', 'Admin'
 */

-- -----------------------------------------------------
-- Table `lite_cms`.`settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lite_cms`.`settings` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

/*

'1', 'LICENSE', 'License Key', '00000000'
'2', 'GOOGLE_CODE', 'Google Analytics Code', 'UA-46001356-1'
'3', 'C_EMAIL', 'Contact Email', 'contact@site.com'
'4', 'C_ADDRESS', 'Address', '123 Name Lane, Chicago IL'
'5', 'C_PHONE', 'Phone', '1-800-555-5555'

 */


-- -----------------------------------------------------
-- Table `lite_cms`.`pages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lite_cms`.`pages` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`pages` (
  `page_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `title` VARCHAR(45) NULL DEFAULT NULL ,
  `enabled` INT(11) NOT NULL DEFAULT '1' ,
  `author_id` INT(11) NOT NULL DEFAULT '1' ,
  `content` TEXT NULL DEFAULT NULL ,
  `create_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ,
  `publish` VARCHAR(10) NOT NULL DEFAULT 'publish',
  `edit_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `meta` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`page_id`) ,
  FOREIGN KEY (`author_id` ) REFERENCES `lite_cms`.`users` (`user_id` )
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

/*
'1', 'index', 'Home Page', '1', '1', '<div class=\"content\">\r\n<div class=\"jumbotron\">\r\n         <h1>LiteCMS <small> a simple OOP PHP CMS</small></h1>\r\n         <p>Manage the content of your Website with ease, using Twitter Bootstrap!</p>\r\n         <p>\r\n           <a class=\"btn btn-lg btn-primary\" href=\"/about\">Learn More <i class=\"fa fa-angle-double-right\"></i></a>\r\n         </p>\r\n       </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n          <h2>Simple</h2>\r\n          <p>Add and Edit pages from a secure Admin Dashboard.  Use the built in <strong>WYSIWYG</strong> (What You See Is What You Get) editor to create HTML5 and Twitter Bootstrap markup.</p>\r\n          <p><a class=\"btn btn-primary\" role=\"button\" href=\"/about\">Learn More <i class=\"fa fa-angle-double-right\"></i></a></p>\r\n        </div>\r\n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n          <h2>Packages</h2>\r\n          <p>Individual License for Small Business and Personal Use.</p>\r\n          <p>Distributed License for Commercial and Remote Use.</p>\r\n          <p>Developer License for <strong>Freelance Web Developers</strong>.</p>\r\n          <p><a class=\"btn btn-primary\" role=\"button\" href=\"/license\">License Details <i class=\"fa fa-angle-double-right\"></i></a></p>\r\n       </div>\r\n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n          <h2>Modules</h2>\r\n          <p>A basic <strong>Blog</strong> modules is included in the <strong>LiteCMS</strong> distribution.  Modules are simple to develop and can add flexibility and features.</p>\r\n          <p><a class=\"btn btn-primary\" role=\"button\" href=\"/modules\">View Details <i class=\"fa fa-angle-double-right\"></i></a></p>\r\n        </div>\r\n      </div>\r\n   </div>', '0000-00-00 00:00:00', 'publish', '0000-00-00 00:00:00', 'LiteCMS Simple PHP OOP CMS: Content Management System'
'2', 'about', 'About', '1', '1', '<div class=\"content\">\r\n<p><strong>LiteCMS</strong> is a simple Content Management System built with PHP Object Oriented Programming in Mind.  It\'s easy to create new pages, edit content, and moderate Users and Accounts.  With the additional <strong>Blog</strong> module template, developers can easily add their own custom modules to suite their sites needs.</p>\r\n<hr>\r\n<h1>Features</h1>\r\n<ul class=\"list-group\">\r\n<li class=\"list-group-item\">Using Twitter Bootstrap 3.0 as scaffolding for both the Frontend and Admin Dashboard for easy, quick website development.</li>\r\n<li class=\"list-group-item\">WYSIWYG Editor that uses Twitter Bootstrap and HTML5 markup, uploads pictures automatically, and lets you write HTML markup directly.</li>\r\n<li class=\"list-group-item\">Routed URIs, clean URLs, and flexible development of dynamic or static content.</li>\r\n<li class=\"list-group-item\">Basic Search Engine Optimization Meta Data that can be expanded on.</li>\r\n<li class=\"list-group-item\">Access to the latest builds and updates for LiteCMS.</li>\r\n</ul>\r\n<div class=\"page-header\">\r\n        <h1>Twitter Bootstrap Examples <small><a href=\"http://www.getbootstrap.com\" target=\"_blank\">See more <i class=\"fa fa-angle-double-right\"></i></a></small></h1>\r\n</div>\r\n<div class=\"page-header\">\r\n        <h1>Buttons</h1>\r\n      </div>\r\n      <p>\r\n        <button class=\"btn btn-lg btn-default\" type=\"button\">Default</button>\r\n        <button class=\"btn btn-lg btn-primary\" type=\"button\">Primary</button>\r\n        <button class=\"btn btn-lg btn-success\" type=\"button\">Success</button>\r\n        <button class=\"btn btn-lg btn-info\" type=\"button\">Info</button>\r\n        <button class=\"btn btn-lg btn-warning\" type=\"button\">Warning</button>\r\n        <button class=\"btn btn-lg btn-danger\" type=\"button\">Danger</button>\r\n        <button class=\"btn btn-lg btn-link\" type=\"button\">Link</button>\r\n      </p>\r\n      <p>\r\n        <button class=\"btn btn-default\" type=\"button\">Default</button>\r\n        <button class=\"btn btn-primary\" type=\"button\">Primary</button>\r\n        <button class=\"btn btn-success\" type=\"button\">Success</button>\r\n        <button class=\"btn btn-info\" type=\"button\">Info</button>\r\n        <button class=\"btn btn-warning\" type=\"button\">Warning</button>\r\n        <button class=\"btn btn-danger\" type=\"button\">Danger</button>\r\n        <button class=\"btn btn-link\" type=\"button\">Link</button>\r\n      </p>\r\n      <p>\r\n        <button class=\"btn btn-sm btn-default\" type=\"button\">Default</button>\r\n        <button class=\"btn btn-sm btn-primary\" type=\"button\">Primary</button>\r\n        <button class=\"btn btn-sm btn-success\" type=\"button\">Success</button>\r\n        <button class=\"btn btn-sm btn-info\" type=\"button\">Info</button>\r\n        <button class=\"btn btn-sm btn-warning\" type=\"button\">Warning</button>\r\n        <button class=\"btn btn-sm btn-danger\" type=\"button\">Danger</button>\r\n        <button class=\"btn btn-sm btn-link\" type=\"button\">Link</button>\r\n      </p>\r\n      <p>\r\n        <button class=\"btn btn-xs btn-default\" type=\"button\">Default</button>\r\n        <button class=\"btn btn-xs btn-primary\" type=\"button\">Primary</button>\r\n        <button class=\"btn btn-xs btn-success\" type=\"button\">Success</button>\r\n        <button class=\"btn btn-xs btn-info\" type=\"button\">Info</button>\r\n        <button class=\"btn btn-xs btn-warning\" type=\"button\">Warning</button>\r\n        <button class=\"btn btn-xs btn-danger\" type=\"button\">Danger</button>\r\n        <button class=\"btn btn-xs btn-link\" type=\"button\">Link</button>\r\n      </p>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Thumbnails</h1>\r\n      </div>\r\n      <img class=\"img-thumbnail\" alt=\"A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera\" src=\"/public/images/square.png\">\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Dropdown menus</h1>\r\n      </div>\r\n      <div class=\"dropdown theme-dropdown clearfix\">\r\n        <a class=\"sr-only dropdown-toggle\" id=\"dropdownMenu1\" role=\"button\" href=\"#\" data-toggle=\"dropdown\">Dropdown <b class=\"caret\"></b></a>\r\n        <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n          <li class=\"active\" role=\"presentation\"><a tabindex=\"-1\" role=\"menuitem\" href=\"#\">Action</a></li>\r\n          <li role=\"presentation\"><a tabindex=\"-1\" role=\"menuitem\" href=\"#\">Another action</a></li>\r\n          <li role=\"presentation\"><a tabindex=\"-1\" role=\"menuitem\" href=\"#\">Something else here</a></li>\r\n          <li class=\"divider\" role=\"presentation\"></li>\r\n          <li role=\"presentation\"><a tabindex=\"-1\" role=\"menuitem\" href=\"#\">Separated link</a></li>\r\n        </ul>\r\n      </div>\r\n\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Navbars</h1>\r\n      </div>\r\n\r\n      <div class=\"navbar navbar-default\">\r\n        <div class=\"container\">\r\n          <div class=\"navbar-header\">\r\n            <button class=\"navbar-toggle\" type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"#\">Project name</a>\r\n          </div>\r\n          <div class=\"navbar-collapse collapse\">\r\n            <ul class=\"nav navbar-nav\">\r\n              <li class=\"active\"><a href=\"#\">Home</a></li>\r\n              <li><a href=\"#about\">About</a></li>\r\n              <li><a href=\"#contact\">Contact</a></li>\r\n              <li class=\"dropdown\">\r\n                <a class=\"dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">Dropdown <b class=\"caret\"></b></a>\r\n                <ul class=\"dropdown-menu\">\r\n                  <li><a href=\"#\">Action</a></li>\r\n                  <li><a href=\"#\">Another action</a></li>\r\n                  <li><a href=\"#\">Something else here</a></li>\r\n                  <li class=\"divider\"></li>\r\n                  <li class=\"dropdown-header\">Nav header</li>\r\n                  <li><a href=\"#\">Separated link</a></li>\r\n                  <li><a href=\"#\">One more separated link</a></li>\r\n                </ul>\r\n              </li>\r\n            </ul>\r\n          </div><!--/.nav-collapse -->\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"navbar navbar-inverse\">\r\n        <div class=\"container\">\r\n          <div class=\"navbar-header\">\r\n            <button class=\"navbar-toggle\" type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"#\">Project name</a>\r\n          </div>\r\n          <div class=\"navbar-collapse collapse\">\r\n            <ul class=\"nav navbar-nav\">\r\n              <li class=\"active\"><a href=\"#\">Home</a></li>\r\n              <li><a href=\"#about\">About</a></li>\r\n              <li><a href=\"#contact\">Contact</a></li>\r\n              <li class=\"dropdown\">\r\n                <a class=\"dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">Dropdown <b class=\"caret\"></b></a>\r\n                <ul class=\"dropdown-menu\">\r\n                  <li><a href=\"#\">Action</a></li>\r\n                  <li><a href=\"#\">Another action</a></li>\r\n                  <li><a href=\"#\">Something else here</a></li>\r\n                  <li class=\"divider\"></li>\r\n                  <li class=\"dropdown-header\">Nav header</li>\r\n                  <li><a href=\"#\">Separated link</a></li>\r\n                  <li><a href=\"#\">One more separated link</a></li>\r\n                </ul>\r\n              </li>\r\n            </ul>\r\n          </div><!--/.nav-collapse -->\r\n        </div>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Alerts</h1>\r\n      </div>\r\n      <div class=\"alert alert-success\">\r\n        <strong>Well done!</strong> You successfully read this important alert message.\r\n      </div>\r\n      <div class=\"alert alert-info\">\r\n        <strong>Heads up!</strong> This alert needs your attention, but it\'s not super important.\r\n      </div>\r\n      <div class=\"alert alert-warning\">\r\n        <strong>Warning!</strong> Best check yo self, you\'re not looking too good.\r\n      </div>\r\n      <div class=\"alert alert-danger\">\r\n        <strong>Oh snap!</strong> Change a few things up and try submitting again.\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Progress bars</h1>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\"><span class=\"sr-only\">60% Complete</span></div>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%;\"><span class=\"sr-only\">40% Complete (success)</span></div>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%;\"><span class=\"sr-only\">20% Complete</span></div>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\"><span class=\"sr-only\">60% Complete (warning)</span></div>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%;\"><span class=\"sr-only\">80% Complete (danger)</span></div>\r\n      </div>\r\n      <div class=\"progress\">\r\n        <div class=\"progress-bar progress-bar-success\" style=\"width: 35%;\"><span class=\"sr-only\">35% Complete (success)</span></div>\r\n        <div class=\"progress-bar progress-bar-warning\" style=\"width: 20%;\"><span class=\"sr-only\">20% Complete (warning)</span></div>\r\n        <div class=\"progress-bar progress-bar-danger\" style=\"width: 10%;\"><span class=\"sr-only\">10% Complete (danger)</span></div>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>List groups</h1>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-4\">\r\n          <ul class=\"list-group\">\r\n            <li class=\"list-group-item\">Cras justo odio</li>\r\n            <li class=\"list-group-item\">Dapibus ac facilisis in</li>\r\n            <li class=\"list-group-item\">Morbi leo risus</li>\r\n            <li class=\"list-group-item\">Porta ac consectetur ac</li>\r\n            <li class=\"list-group-item\">Vestibulum at eros</li>\r\n          </ul>\r\n        </div><!-- /.col-sm-4 -->\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"list-group\">\r\n            <a class=\"list-group-item active\" href=\"#\">\r\n              Cras justo odio\r\n            </a>\r\n            <a class=\"list-group-item\" href=\"#\">Dapibus ac facilisis in</a>\r\n            <a class=\"list-group-item\" href=\"#\">Morbi leo risus</a>\r\n            <a class=\"list-group-item\" href=\"#\">Porta ac consectetur ac</a>\r\n            <a class=\"list-group-item\" href=\"#\">Vestibulum at eros</a>\r\n          </div>\r\n        </div><!-- /.col-sm-4 -->\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"list-group\">\r\n            <a class=\"list-group-item active\" href=\"#\">\r\n              <h4 class=\"list-group-item-heading\">List group item heading</h4>\r\n              <p class=\"list-group-item-text\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>\r\n            </a>\r\n            <a class=\"list-group-item\" href=\"#\">\r\n              <h4 class=\"list-group-item-heading\">List group item heading</h4>\r\n              <p class=\"list-group-item-text\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>\r\n            </a>\r\n            <a class=\"list-group-item\" href=\"#\">\r\n              <h4 class=\"list-group-item-heading\">List group item heading</h4>\r\n              <p class=\"list-group-item-text\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>\r\n            </a>\r\n          </div>\r\n        </div><!-- /.col-sm-4 -->\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Panels</h1>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"panel panel-default\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n          <div class=\"panel panel-primary\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n        </div><!-- /.col-sm-4 -->\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"panel panel-success\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n          <div class=\"panel panel-info\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n        </div><!-- /.col-sm-4 -->\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"panel panel-warning\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n          <div class=\"panel panel-danger\">\r\n            <div class=\"panel-heading\">\r\n              <h3 class=\"panel-title\">Panel title</h3>\r\n            </div>\r\n            <div class=\"panel-body\">\r\n              Panel content\r\n            </div>\r\n          </div>\r\n        </div><!-- /.col-sm-4 -->\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"page-header\">\r\n        <h1>Wells</h1>\r\n      </div>\r\n      <div class=\"well\">\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>\r\n      </div>\r\n\r\n\r\n</div><small> <!-- /container --></small>', '0000-00-00 00:00:00', 'publish', '2013-11-11 20:43:18', 'Learn more about LiteCMS, a Simple PHP OOP CMS - Content Management System'
'3', 'contact', 'Contact', '1', '1', NULL, '0000-00-00 00:00:00', 'publish', '0000-00-00 00:00:00', 'LiteCMS Contact Form'
'4', 'privacy', 'Privacy Policy', '1', '1', '<div class=\"content\"><h3>Privacy Policy</h3><div><br></div><div>Your policy information here...</div></div>', '0000-00-00 00:00:00', 'publish', '0000-00-00 00:00:00', 'LiteCMS Privacy Policy'
'5', 'profile', 'User Profile', '0', '1', '', '0000-00-00 00:00:00', 'publish', '0000-00-00 00:00:00', 'LiteCMS User Profile'
 */

-- -----------------------------------------------------
-- Table `lite_cms`.`page_logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lite_cms`.`page_logs` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`page_logs` (
  `log_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `page_id` INT(11) NOT NULL ,
  `message` VARCHAR(255) NOT NULL ,
  `log_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`log_id`) ,
  FOREIGN KEY (`page_id` ) REFERENCES `lite_cms`.`pages` (`page_id` )
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lite_cms`.`user_logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lite_cms`.`user_logs` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`user_logs` (
  `log_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL DEFAULT '0' ,
  `log_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`log_id`) ,
  FOREIGN KEY (`user_id` ) REFERENCES `lite_cms`.`users` (`user_id` )
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `lite_cms`.`blog_articles`
-- -----------------------------------------------------
/*
DROP TABLE IF EXISTS `lite_cms`.`blog_articles` ;

CREATE  TABLE IF NOT EXISTS `lite_cms`.`blog_articles` (
  `article_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `create_date` DATETIME NOT NULL ,
  `edit_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `publish` TINYINT(4) NOT NULL DEFAULT '1' ,
  `author_id` INT(11) NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `content` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`article_id`) ,
  FOREIGN KEY (`author_id` ) REFERENCES `lite_cms`.`users` (`user_id` )
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;
*/

-- -----------------------------------------------------
-- Table `lite_cms`.`publisher_articles`
-- -----------------------------------------------------
/*
DROP TABLE IF EXISTS `lite_cms`.`publisher_articles` ;

CREATE TABLE `publisher_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `edit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish` tinyint(4) NOT NULL DEFAULT '1',
  `author_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`article_id`),
  FOREIGN KEY (`author_id` ) REFERENCES `lite_cms`.`users` (`user_id` )
) ENGINE=InnoDB 
AUTO_INCREMENT = 1 
DEFAULT CHARSET = utf8;
*/


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


