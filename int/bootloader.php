<?php
/**
 * Bootloader
 * 
 * Filename: bootloader.php
 * Description: Initializes classes and objects for the site
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

//Load Config Constants
require_once('config.php');

//Autoload Class and files
spl_autoload_register(function ($class) {
	$path = $class.".php";
    require_once($path);
});

//Initiate DB Object
//$db_type, $db_name, $db_host, $db_user, $db_pass = ''
$dBase = new CRUD(DATABASE_TYPE, DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD);

//Grab Dynamic Settings
$dBase->table = "settings";
$settings = $dBase->select(array('name', 'value'));

foreach($settings as $setting){
	defined($setting['name'])
		|| define($setting['name'], $setting['value']);
}

//Initiate URI
Url::getInstance();

//Initialize User
User::getInstance($dBase);

//Setup routes
$router = new Router();

//Load routed pages from page directory
$dir = ROOT_PATH . DS . PAGES_DIR;
$dir = scandir($dir);

foreach ($dir as $key => $value){
	if(stripos($value, '.php')){
		$router->add(preg_replace("/.php/", '', $value));
	}
}

//Admin Routes
$router->add('dashboard');
$router->add('help');

//Initiate Core Class 
$core = new Core($router, $dBase);

/** EOF */