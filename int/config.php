<?php
/**
 * Configuraiton File
 *
 * Filename: config.php
 * Description: Contains the configuration and constants to run the CMS
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

if(!isset($_SESSION)) {
	session_start();
}

//Site domain name with http
defined("SITE_URL")
	|| define("SITE_URL", "http://".$_SERVER['SERVER_NAME']);

//Site Name 
defined("SITE_NAME")
	|| define("SITE_NAME", $_SERVER['SERVER_NAME']);

//Directory Seperator
defined("DS")
	|| define("DS", DIRECTORY_SEPARATOR);

//Root Path
defined("ROOT_PATH")
	|| define("ROOT_PATH", realpath(dirname(__FILE__) . DS . ".." . DS));

//Default Route
defined("DEFAULT_ROUTE")
	|| define("DEFAULT_ROUTE", "index");

//Default Contact Email
defined("CONTACT_EMAIL")
	|| define("CONTACT_EMAIL", "contact@".$_SERVER['SERVER_NAME']);

//Classes Folder
defined("CLASSES_DIR")
	|| define("CLASSES_DIR", "classes");

//Pages Folder
defined("PAGES_DIR")
	|| define('PAGES_DIR', "pages");

//Module Folder
defined("MODULE_DIR")
	|| define("MODULE_DIR", "modules");

//Inc Folder
defined("INT_DIR")
	|| define("INT_DIR", "int");

//Template Folder
defined("TEMPLATE_DIR")
	|| define("TEMPLATE_DIR", PAGES_DIR . DS . "views". DS ."templates");

//Public Folder
defined("PUBLIC_DIR")
	|| define("PUBLIC_DIR", "public");

//Uploads Folder
defined("UPLOADS_DIR")
	|| define("UPLOADS_DIR", PUBLIC_DIR . DS . "images" . DS . "uploads");

//Admin Folder
defined("ADMIN_DIR")
	|| define("ADMIN_DIR", "admin");

//Add all directories to include path
set_include_path(implode(
	PATH_SEPARATOR, array(
		realpath(ROOT_PATH . DS . CLASSES_DIR),
		realpath(ROOT_PATH . DS . PAGES_DIR),
		realpath(ROOT_PATH . DS . MODULE_DIR),
		realpath(ROOT_PATH . DS . INT_DIR),
		realpath(ROOT_PATH . DS . TEMPLATE_DIR),
		realpath(ROOT_PATH . DS . PUBLIC_DIR),
		realpath(ROOT_PATH . DS . ADMIN_DIR),
		get_include_path()
		)
	));

//Define Database Connection
defined("DATABASE_TYPE")
	|| define("DATABASE_TYPE", 'mysql');

defined("DATABASE_NAME")
	|| define("DATABASE_NAME", 'lite_cms');

defined("DATABASE_HOST")
	|| define("DATABASE_HOST", 'localhost');

defined("DATABASE_USER")
	|| define("DATABASE_USER", 'root');

defined("DATABASE_PASSWORD")
	|| define("DATABASE_PASSWORD", '');

//User Role Enums
class Enum {

	//User Roles
	const ADMIN = 0;
	const USER = 1;
}
	
/** EOF */