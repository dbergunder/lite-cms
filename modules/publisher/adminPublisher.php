<?php
/**
 * Publisher Module Admin Class
 * 
 * Filename: adminPublisher.php
 * Description: Publisher Module
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class adminPublisher extends dashboard {

	//Call Parent Dashboard Constructor
	public function __construct(){
		parent::__construct();
	}

	//Loads view of all articles
	public function loadContent(){
		$this->_dBase->table = "publisher_articles";
		$articles = $this->_dBase->select(array(
											'article_id', 
											'title', 
											'author_id', 
											'create_date', 
											'edit_date', 
											'publish'
											));

		require_once(ROOT_PATH . DS. MODULE_DIR . DS . 'publisher' . DS . 'views'. DS . __CLASS__ .'.phtml');
	}

	//Adds an article
	public function addArticle(){
		//Set Database call to blog table
		$this->_dBase->table = 'publisher_articles';

		//If we are posting a new article
		if($_POST){
			//Title field set
			if(!empty($_POST['title'])){

				//Insert into DB
				$this->_dBase->insert(array(
										'title' =>  $_POST['title'], 
										'author_id' => User::getId(), 
										'create_date' => date("Y-m-d H:i:s"),
										'description' => $_POST['description'],
										'content' =>  $this->sanitize($_POST['content'])
										));

				//Load adminBlog.phtml
				$this->loadContent();
				return true;
			}
			else{
				$this->_error = 'Published articles must have a Title';
			}
		}

		require_once(ROOT_PATH . DS. MODULE_DIR . DS . 'publisher' . DS . 'views'. DS . __CLASS__ .'_'.__FUNCTION__.'.phtml');
	}

	//Edit article
	public function editArticle(){
		//Set Database to blog table
		$this->_dBase->table = 'publisher_articles';
		//Get URL parameters
		$params = Url::getAll();
		//User Params
		$article = '';

		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check to see if its in the database
			$query = $this->_dBase->select(array('title', 'description', 'content'), array('article_id' => $params['id']));
			//Make sure DB entry exists
			if(isset($query[0])){
				$article = $query[0];

				if($_POST){
					//Title can not be blank.
					if($_POST['title'] != ''){
						//Update content
						$this->_dBase->update(array(
												'title' => $_POST['title'], 
												'content' => $_POST['content'],
												'description' => $_POST['description']) , 
												array('article_id' => $params['id'])
												);

						$this->_success = "Article Updated";
						$this->loadContent();
						return true;

					}
					else{
						$this->_error = "Title can not be blank.";
					}
				}

				require_once(ROOT_PATH . DS. MODULE_DIR . DS . 'publisher' . DS . 'views'. DS . __CLASS__ .'_'.__FUNCTION__.'.phtml');
			}
			else{
				$this->_error = "Article ID not found.";
				$this->loadContent();
			}
		}
		else{
			$this->_error = "Invalid parameters passed to Edit Article";
			$this->loadContent();
		}
	}

	//Toggle publishing of article
	public function toggleArticle(){
		//Set Database call to blog table
		$this->_dBase->table = 'publisher_articles';
		//Check for params passed via Url
		$params = Url::getAll();

		//Check to see if params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			//Toggle Publish from DB
			$query = $this->_dBase->select(array('publish', 'title'), array('article_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Article could not be located.";
			}
			else{
				//Toggle
				switch($query[0]['publish']) {
					case 0:
						//Update to 1
						$this->_dBase->update(array('publish' => 1), array('article_id' => $params['id']));

						//Success
						$this->_success = $query[0]['title']. " article enabled.";
						break;
					case 1:
						//Update to 0
						$this->_dBase->update(array('publish' => 0), array('article_id' => $params['id']));

						//Success
						$this->_success = $query[0]['title']. " article disabled.";
						break;
					default:
						$this->_error = "Problem updating Article ". $query[0]['title'];
				}
			}
		}
		else{
			$this->_error = "Invalid parameters for toggling Article.";
		}

		$this->loadContent();
	}

	//Delete article
	public function deleteArticle(){
		$this->_dBase->table = 'publisher_articles';
		$params = Url::getAll();

		//Make sure params are set, not empty, and ID is numeric
		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){
			//Check for Instance in Database
			$query = $this->_dBase->select(array('title'), array('article_id' => $params['id']));

			//Make sure DB entry exists
			if(!isset($query[0])){
				$this->_error = "Article ID not found.";
			}
			else{
				//Delete user from Database
				$this->_dBase->delete(array('article_id' => $params['id']));
				$this->_success = $query[0]['title']. " succesfully deleted.";
			}
		}
		else{
			$this->_error = "Invalid parameters for Article deletion.";
		}
		//Return to dashboard_users
		$this->loadContent();
	}
}

/** EOF */