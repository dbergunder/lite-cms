<?php
/**
 * Publisher Module Class
 * 
 * Filename: publisher.php
 * Description: Article Publisher Module
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class publisher extends Page {

	public function __construct(){
		parent::__construct();
		$this->_title = "Articles";
	}

	//Show all articles
	public function loadContent(){
		$this->_params = Url::getAll();

		$this->_dBase->table = "publisher_articles";
		$articles = $this->_dBase->select(array(
											'article_id', 
											'title', 
											'description', 
											'author_id', 
											'create_date'), 
										array('publish' => 1));

		$this->_dBase->table = "users";
		$users = $this->_dBase->select(array('user_id', 'user_name'));

		$archive = array();

		foreach($articles as $key => &$value){
		//while (list($key, $value) = each($articles)){
			//Update author name
			foreach($users as $user => $row){
				if($value['author_id'] == $row['user_id']){
					$value['author_name'] = $row['user_name'];
				}
			}

			//Generate Archive year list
			$archive[date('Y', strtotime($value['create_date']))] = date('Y', strtotime($value['create_date']));

			//If Archive year is submitted, only return those years
			if(!empty($this->_params['year'])){
				if(date('Y', strtotime($value['create_date'])) != $this->_params['year']){
					unset($articles[$key]);
				}
			}	
		}unset($value); //Fix foreach reference bug

		require_once(ROOT_PATH . DS. MODULE_DIR . DS .  __CLASS__ . DS . 'views'. DS . __CLASS__ .'.phtml');
	}

	//Display individual article
	public function viewArticle(){
		$this->_dBase->table = "publisher_articles";
		$params = Url::getAll();
		//User Params
		$article = '';

		if (isset($params['id']) && $params['id'] != '' && is_numeric($params['id'])){

			$article = $this->_dBase->select(array(
												'article_id', 
												'title', 
												'description', 
												'content', 
												'author_id', 
												'create_date'), 
											array('publish' => 1, 'article_id' => $params['id']));
			$article = $article[0];

			$this->_dBase->table = "users";
			$user = $this->_dBase->select(array('user_name'), array('user_id' => $article['author_id']));
			$user = $user[0];

			$article['author_name'] = $user['user_name'];

			//Add article title to page header
			$this->_title = $this->_title ." | ". $article['title'];
		}
		else{
			$this->_error = "Article could not be found.";
		}

		require_once(ROOT_PATH . DS. MODULE_DIR . DS .  __CLASS__ . DS . 'views'. DS . __CLASS__ .'_'.__FUNCTION__.'.phtml');
	}
}

/** EOF */