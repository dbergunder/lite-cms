<?php
/**
 * Publisher Feed Module Class
 * 
 * Filename: publisherFeed.php
 * Description: Feed for Article Publisher Module
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

class publisherFeed extends Page {

	private $_xml = '';

	public function __construct(){
		parent::__construct();
		$this->_dBase->table = "publisher_articles";
		$articles = $this->_dBase->select(array('article_id', 'title', 'content', 'author_id', 'create_date'), array('publish' => 1));

		//title, content (truncated), create_date (formated), url
	    $this->_xml = '<?xml version="1.0" encoding="ISO-8859-1" ?><rss version="2.0"><channel><title>'.SITE_NAME.' Blog RSS</title><link>'.SITE_URL.'/publisherFeed/rss</link><description>Blog Feed</description><language>English</language>';
		foreach($articles as $key => $value) {
			 $this->_xml .=  '<item>';
	         $this->_xml .=     '<title>'.$value['title'].'</title>';
	         $this->_xml .=     '<link>'.SITE_URL.'/publisher/viewArticle/id/'.$value['article_id'].'</link>';
	         $this->_xml .=     '<description><![CDATA['.$value['content'].']]></description>';
	         $this->_xml .=	 	'<pubDate>'.$value['create_date'].'</pubDate>';
			 $this->_xml .=	 '</item>';
		}
		$this->_xml .= '</channel></rss>';
	}

	//Overload Load functions for no output.
	protected function loadContent(){}

	//Set header content type to xml
	protected function loadHeader(){
		header("Content-type: text/xml; charset=utf-8");
	}

	//Overload Load functions for no output.
	protected function loadFooter(){}

	//Overload Load functions for no output.
	protected function loadNav(){}

	//Return RSS
	public function rss(){
		echo $this->_xml;
	}
}

/** EOF */