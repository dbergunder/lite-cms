<?php

class ajax extends Page {

	public function __construct(){
		parent::__construct();
	}

	//Overload Load functions for no output.
	protected function loadContent(){
		//Force goto homepage?
	}

	protected function loadHeader(){}

	protected function loadFooter(){}

	protected function loadNav(){}

	//Stores in public/images/upload
	//Returns Image URL
	public function saveImage(){
		if(isset($_FILES['file'])){

			//Allow folder to write
			chmod(ROOT_PATH.DS.UPLOADS_DIR, 0777);

	        $destination = ROOT_PATH.DS.UPLOADS_DIR.DS.$_FILES['file']['name'];
	        $location =  $_FILES["file"]["tmp_name"];

	        //Try to move file
	        if(move_uploaded_file($location,$destination)){
	        	//Return URL location
				echo SITE_URL.'/public/images/uploads/'.$_FILES['file']['name'];
	        }
	        else{
	        	echo 'failure';
	        }
	        //Lock folder to write.
	        chmod(ROOT_PATH.DS.UPLOADS_DIR, 0644);
		}
	}

}

 /** EOF */ 