<?php

class contact extends Page {

	public function __construct(){
		parent::__construct();

		$this->_js[] = '<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>';
		$this->_js[] = '<script src="/public/js/bootstrap-contact.js"></script>';
	}

	protected function loadContent(){
		//parent::loadContent();
		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}

	//Submit Contact Form Handler
	public function submit(){
		//Check for post dump
		if($_POST){
			//Name, Email, Message, and Phone must be set
			if(isset($_POST['contactname']) && isset($_POST['email']) && isset($_POST['message']) && isset($_POST['phone'])){

				//Create Message
				$message = "New Message from: ". $_POST['contactname'] .", ". $_POST['email']. "\n";
				$message .="\n";
				$message .="Phone: ". $_POST['phone'] ."\n";
				$message .= $_POST['message'];

				//Setup Mailer Object
				$email = new Mailer();
				$email->addRecipient('Contact Form', CONTACT_EMAIL);
				$email->setFrom($_POST['contactname'], $_POST['email']);
				$email->fillSubject("Contact form ". $_POST['contactname'] .".");
				$email->fillMessage($message);

				//Send
				if($email->send()){
					$this->_success = "Your message has been submitted.";
				}
			}
			else{
				$this->_error = "You must fill out all required fields";
			}
		}
		$this->loadContent();	
	}

}

 /** EOF */ 