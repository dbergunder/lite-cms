<?php

class login extends Page {

	public function __construct(){
		parent::__construct();
		$this->_title = "Account Login";
	}

	protected function loadContent(){
		//parent::loadContent();
		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}

	public function submit(){
		//Check for post dump
		if($_POST){
			if(User::login($_POST['email'], $_POST['password']))
			{
				switch(User::getRole()){
					case Enum::ADMIN:
						//Admin Role Go to Dashboard
						Helper::redirect(SITE_URL."/"."dashboard/");
						break;
					case Enum::USER:
						//User Role Go to Profile
						Helper::redirect(SITE_URL."/"."profile/");				
						break;
				}
			}
			else{
				$this->_error = "Invalid Email or Password";
			}
		}
	}

	public function logout(){
		User::logout();
		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}

}