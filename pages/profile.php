<?php

class profile extends Page {

	protected $_role = Enum::USER;

	public function __construct(){
		//Check for user loggin/Authorization before calling parent construct
		if(User::is_logged()){
			//Check user role
			$auth = new Auth(User::getRole());

			if (!$auth->hasAuth($this->_role) && (User::getRole() != Enum::ADMIN)){
				$this->_error = "Account does not have User privledges";
				//Load the home page
				$page = new index();
				$page->view();
				//Stop the current page from loading
				exit();
			}
		}
		else{
			//GoTo login
			$page = new login();
			$page->view();
			//Stop the current page from loading
			exit();
		}

		parent::__construct();
	}

	public function loadContent(){
		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}
}

 /** EOF */ 