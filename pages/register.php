<?php

class register extends Page {

	public function __construct(){
		parent::__construct();
		$this->_title = "Account Registration";
	}

	protected function loadContent(){
		//Overwrite content from parent
		//parent::loadContent();
		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}

}

 /** EOF */ 