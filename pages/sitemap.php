<?php

class sitemap extends Page {

	public function __construct(){
		parent::__construct();
		$this->_title = "Sitemap";
	}

	protected function loadContent(){
		global $router;
		$links = $router->getRoutes();

		//Blacklisted pages
		$list = array(
					'error',
					'dashboard',
					'help',
					'ajax',
					'index',
					'sitemap'
					);

		//Check Database for disabled pages
		$this->_dBase->table = 'pages';
		$query = $this->_dBase->select('name', array('enabled' => 0));
		//Add to blacklist
		if(isset($query[0])){
			$disabled = $query[0];
			$list = array_merge($list, $disabled);
		}
		
		//Remove list from links array
		$links = array_diff($links, $list);

		//Remove any module Admin links
		//TODO: Redesign or better option?
		$links = preg_grep("/admin/", $links, PREG_GREP_INVERT);

		require_once(ROOT_PATH.'/pages/views/'.__CLASS__.'.phtml');
	}

}

 /** EOF */ 