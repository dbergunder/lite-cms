<?php

class sitemapXML extends Page {

	protected $_xml;

	public function __construct(){
		parent::__construct();
	}

	protected function loadContent(){
		global $router;
		$links = $router->getRoutes();

		//Blacklisted pages
		$list = array(
					'error',
					'dashboard',
					'help',
					'ajax',
					'index',
					'sitemapXML'
					);

		//Check Database for disabled pages
		$this->_dBase->table = 'pages';
		$query = $this->_dBase->select('name', array('enabled' => 0));
		//Add to blacklist
		if(isset($query[0])){
			$disabled = $query[0];
			$list = array_merge($list, $disabled);
		}
		
		//Remove list from links array
		$links = array_diff($links, $list);

		//Remove any module Admin links
		//TODO: Redesign or better option?
		$links = preg_grep("/admin/", $links, PREG_GREP_INVERT);

		//title, content (truncated), create_date (formated), url
	    $this->_xml = '<?xml version="1.0" encoding="ISO-8859-1" ?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		foreach($links as $key => $value) {
			 $this->_xml .=  '<url>';
	         $this->_xml .=     '<loc>'.SITE_URL.'/'.$value.'</loc>';
	         $this->_xml .=     '<changefreq>monthly</changefreq>';
			 $this->_xml .=	 '</url>';
		}
		$this->_xml .= '</urlset>';

		echo $this->_xml;
	}

	//Set header content type to xml
	protected function loadHeader(){
		header("Content-type: text/xml; charset=utf-8");
	}

	//Overload Load functions for no output.
	protected function loadFooter(){}

	//Overload Load functions for no output.
	protected function loadNav(){}

}

 /** EOF */ 