<?php
/**
 * CMS Start
 * 
 * Filename: index.php
 * Description: CMS Index Page, .htaccess points here
 * @version: 1.0.0
 * @author David Bergunder <dbergunder@gmail.com>
 * @copyright Copyright (c) 2013 David Bergunder
 *
 */

//require_once('../int/autoload.php');
require_once('../int/bootloader.php');

//Catch anything before running the core site

//Run the site
$core->run();

/** EOF */