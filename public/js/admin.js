
//***********************************************************************************************

//Updates HTML to textarea for POST
var postForm = function() {

    //Force Editor out of Code view for correctly saving data.
    if($('i.icon-code').parent().hasClass('active')) {
        $('i.icon-code').click();
    }

    var content = $('textarea[name="content"]').val($('#content').code());
};

//***********************************************************************************************

$(document).ready(function() {

    //Initialize the WYSIWYG editor
    $('#content').summernote({
        height: 600,
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0],editor,welEditable);
        }
    });

//***********************************************************************************************

    //Overwrite image insert for upload
    function sendFile(file,editor,welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "/ajax/saveImage",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                    editor.insertImage(welEditable, url);
            },
            error: function(msg) {
                    console.log(msg);
            }
        });
    };

//***********************************************************************************************

    //Table pagination
    $('table#paginated').each(function() {
        var currentPage = 0;
        var numPerPage = $(this).attr('data-item');
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');

        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<ul class="pagination pagination-sm pull-right"></ul>');

        for (var page = 0; page < numPages; page++) {
            $('<li></li>').html('<a href="#">'+ (page + 1) +'</a>').bind('click', {
                newPage: page
            }, function(event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager);
        }
        $pager.appendTo("div#pager").find('li:first').addClass('active');
    });

//***********************************************************************************************

    //When the document is loaded, watch for click event on link
    $(document).on("click", "#deleteLink", function() {
      //Reference id from data
      var pageId = $(this).data('id');
      var href = $('#modalConfirm').attr("href");
      $('#modalConfirm').attr("href", href + pageId);
    });

//***********************************************************************************************

    $('#role').change(function(){
        var $selected = $(this).find(':selected');

        $('#description').html($selected.data('description'));
    }).trigger('change'); // run handler immediately

//***********************************************************************************************

    //Timeout alerts
    setTimeout(function() {
        $('.alert').fadeOut();
    }, 5000);

//***********************************************************************************************

});
